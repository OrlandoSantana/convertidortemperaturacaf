package facci.orlandosantana.convertortem;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    Button buttonFaC;
    Button buttonCaF;
    EditText EditTextingreso;
    TextView textViewR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonFaC = (Button) findViewById(R.id.buttonFC);
        buttonCaF = (Button) findViewById(R.id.buttonCF);
        EditTextingreso = (EditText) findViewById(R.id.editTextM);
        textViewR = (TextView) findViewById(R.id.textViewresultadoconversion);

        buttonFaC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float valor = Float.parseFloat(EditTextingreso.getText().toString());
                float resultadoC = (float) ((valor - 32)/(1.8));
                textViewR.setText(String.valueOf(resultadoC+"°C"));

            }
        });

        buttonCaF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float valor = Float.parseFloat(EditTextingreso.getText().toString());
                float resultadoF = (float) ((valor * 1.8) + 32);
                textViewR.setText(String.valueOf(resultadoF+"°F"));
            }
        });
    }
}
